package sk.koncz.classifier;

import sk.koncz.classifier.datamodel.Configuration;

public abstract class GeneralClassifier {
	protected Configuration configuration;
	protected void setConfiguration(Configuration configuration) throws Exception{
		this.configuration=configuration;
		if (!(configuration.getWeightingMethod().equals("OWN")||configuration.getWeightingMethod().equals("IG"))) {
			throw new Exception("The method must be OWN or IG!"); 
		}
		if (!(configuration.getMaxNGramSize()>0&&configuration.getMaxNGramSize()<6)) {
			throw new ArithmeticException("The maxNGramSize should be from interval <1;5>!"); 
		}
		if ((configuration.getMaxWeightThreshold()<0||configuration.getMinWeightThreshold()>0)) {
			throw new ArithmeticException("The maxWeightThreshold should be from interval <0;1> and the minWeightThreshold from interval <0;-1>!"); 
		}
		if (!(configuration.getSolver().equals("L1R_LR")||configuration.getSolver().equals("L2R_LR"))) {
			throw new Exception("The solver must be L1R_LR or L2R_LR!"); 
		}
		if (configuration.getHighlightText()&&!configuration.getFilterOverliedNGrams()) {
			throw new Exception("The text highlighting works only with enabled filterring of overlalied NGrams!");
		}
	}
	public Configuration getConfiguration(){
		return configuration;
	}
}
