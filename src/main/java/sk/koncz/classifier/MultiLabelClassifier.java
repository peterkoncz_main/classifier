package sk.koncz.classifier;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import sk.koncz.classifier.datamodel.Configuration;
import sk.koncz.classifier.datamodel.Corpus;
import sk.koncz.classifier.datamodel.Document;
import sk.koncz.classifier.datamodel.Model;
import sk.koncz.classifier.datamodel.MultiLabelCorpus;
import sk.koncz.classifier.datamodel.MultiLabelDocument;
import sk.koncz.classifier.datamodel.MultiLabelModel;
import sk.koncz.classifier.datamodel.MultiLabelPrediction;
import sk.koncz.classifier.datamodel.StringNGram;

public class MultiLabelClassifier extends GeneralClassifier{
	private MultiLabelCorpus originalCorpus;
	private List<Corpus> multiLabeledCorpora = new ArrayList<Corpus>();
	private List<Model> multiLabeledModels = new ArrayList<Model>();
	private HashMap<String, List<MultiLabelPrediction>> predictions;
	private HashMap<Integer, Integer> corporaStatistics;
	
	/**
	 * Constructor of the MultiLabelClassifier
	 * @param configuration
	 * @throws Exception
	 */
	public MultiLabelClassifier(Configuration configuration) throws Exception {
		setConfiguration(configuration);
	}
	/**
	 * Constructor of the MultiLabelClassifier
	 * @throws Exception
	 */
	public MultiLabelClassifier() throws Exception {
		setConfiguration(new Configuration());
	}
	/**
	 * Method build models
	 * @param corpus
	 * @return
	 * @throws Exception
	 */
	public List<Model>  buildModel(MultiLabelCorpus corpus) throws Exception {
		this.originalCorpus = corpus;
		getCorporaStatistics();
		generateLabeledCorporaReSampling();
		trainModels();
		this.multiLabeledCorpora = null;
		return multiLabeledModels;
	}
	public List<Model>  buildMetaModel(MultiLabelCorpus corpus) throws Exception {
		this.originalCorpus = corpus;
		getCorporaStatistics();
		generateLabeledCorporaReSampling();
		trainModels();
		addClassAttributes();
		trainModels();
		return multiLabeledModels;
	}
	private void addClassAttributes() {
		/*
		//get the list of classes for each document
		HashMap<String, List<StringNGram>> documentClasses = new HashMap<String, List<StringNGram>>();
		for (Corpus corpus : multiLabeledCorpora) {
			for (Document document : corpus.getDocuments()) {
				if (document.getModelBasedClass()==1) {
					List<StringNGram> currentList = (documentClasses.containsKey(document.getId()))? documentClasses.get(document.getId()): new ArrayList<StringNGram>();
					currentList.add(new StringNGram("specialClassNGram"+corpus.getLabel(), new ArrayList<Integer>(-1)));
					documentClasses.put(document.getId(), currentList);
				}
			}
		}
		//add the labels into the ngrams
		for (Corpus corpus : multiLabeledCorpora) {
			for (Document document : corpus.getDocuments()) {
				if (documentClasses.containsKey(document.getId())) {
					document.getStringNGrams().addAll(documentClasses.get(document.getId()));
				}
			}
		}
		*/
		for (Corpus corpus : multiLabeledCorpora) {
			for (Document document : corpus.getDocuments()) {
				if (document.getModelBasedClass()==1) {
					document.setText(document.getText()+" specialClassNGram"+corpus.getLabel());
				}
			}
		}
	}
	/**
	 * Method to apply models
	 * @param corpus
	 * @param models
	 * @throws Exception
	 */
	public void applyModel(MultiLabelCorpus corpus, List<Model> models) throws Exception {
		this.originalCorpus = corpus;
		this.multiLabeledModels = models;
		getPredictions();
		mergePredictions();
	}
	public void applyMetaModel(MultiLabelCorpus corpus, List<Model> models) throws Exception {
		this.originalCorpus = corpus;
		this.multiLabeledModels = models;
		getPredictions();
		mergePredictions();
		addClassAttributes();
		getPredictions();
		mergePredictions();
	}
	/**
	 * Method to save models
	 * @param model
	 * @param path
	 */
	public void saveModel(List<MultiLabelModel> model, String path){
		try
		{
			FileOutputStream fileOut = new FileOutputStream(path);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(model);
			out.close();
			fileOut.close();
		}catch(IOException i)
		{
			i.printStackTrace();
		}
	}
	/**
	 * Method to load saved models
	 * @param path
	 * @return
	 */
	public List<MultiLabelModel> loadModel(String path){
		List<MultiLabelModel> model = null;
		try
		{
			FileInputStream fileIn = new FileInputStream(path);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			model = (List<MultiLabelModel>) in.readObject();
			in.close();
			fileIn.close();
		}catch(IOException i)
		{
			i.printStackTrace();
			return model;
		}catch(ClassNotFoundException c)
		{
			System.out.println("Model class not found");
			c.printStackTrace();
			return model;
		}
		return model;
	}
	/**
	 * Get the numbers of occurrences for each label
	 * @throws Exception
	 */
	private void getCorporaStatistics() throws Exception{
		HashMap<Integer, Integer> corporaStatistics = new HashMap<Integer, Integer>();
		for (MultiLabelDocument doc : originalCorpus.getDocuments()) {
			for (Integer docClass : doc.getDocClasses()) {
				corporaStatistics.put(docClass, corporaStatistics.containsKey(docClass)?corporaStatistics.get(docClass)+1:1);
			}
		}
		for (int label : corporaStatistics.keySet()) {
			if (label<10) {
				throw new Exception("Each class should contain at least 10 examples!"); 
			}
		}
		this.corporaStatistics=corporaStatistics;
	}
	/**
	 * This method generates multiple binominal corpora 
	 */
	private void generateLabeledCorporaReSampling() {
		for (Integer label : corporaStatistics.keySet()) {
			int size = corporaStatistics.get(label);
			int sizeOfRemainingClasses = originalCorpus.getDocuments().size()-size;
			double ratio = (size>sizeOfRemainingClasses)? size/sizeOfRemainingClasses : sizeOfRemainingClasses/size;
			int biggerClass = (size>sizeOfRemainingClasses)? 1 : 0;
			List<Document> documentsOfSmallerClass = new ArrayList<Document>();
			List<Document> documentsOfBiggerClass = new ArrayList<Document>();
			//We create two groups of documents on for the smaller class and one for the bigger
			for (MultiLabelDocument multiLabelDocument : originalCorpus.getDocuments()) {
				Document document = new Document(multiLabelDocument.getText());
				document.setId(multiLabelDocument.getId());
				document.setDocClass((multiLabelDocument.getDocClasses().contains(label))? 1:0);
				if (biggerClass == document.getDocClass()) {
					documentsOfBiggerClass.add(document);
				}
				else {
					documentsOfSmallerClass.add(document);
				}
			}
			//for each label we will generate Math.ceil(ratio) corpora
			for (int i = 0; i < Math.ceil(ratio); i++) {
				Corpus corpus = new Corpus();
				corpus.setLabel(label);
				List<Document> corpusDocuments = new ArrayList<Document>();
				corpusDocuments.addAll(documentsOfSmallerClass);
				int lastIndex = (documentsOfBiggerClass.size()<(i+1)*documentsOfSmallerClass.size())? documentsOfBiggerClass.size():(i+1)*documentsOfSmallerClass.size();
				corpusDocuments.addAll(documentsOfBiggerClass.subList(i*documentsOfSmallerClass.size(), lastIndex));
				corpus.setDocuments(corpusDocuments);
				multiLabeledCorpora.add(corpus);
			}
			//this is a binary classification task we don't need to create the same corpora twice
			if (corporaStatistics.keySet().size()==2) {
				break;
			}
		}
	}
	private void generateLabeledCorporaSubSampling() {
		for (Integer label : corporaStatistics.keySet()) {
			int size = corporaStatistics.get(label);
			int sizeOfRemainingClasses = originalCorpus.getDocuments().size()-size;
			int biggerClass = (size>sizeOfRemainingClasses)? 1 : 0;
			List<Document> documentsOfSmallerClass = new ArrayList<Document>();
			List<Document> documentsOfBiggerClass = new ArrayList<Document>();
			//We create two groups of documents on for the smaller class and one for the bigger
			for (MultiLabelDocument multiLabelDocument : originalCorpus.getDocuments()) {
				Document document = new Document(multiLabelDocument.getText());
				document.setId(multiLabelDocument.getId());
				document.setDocClass((multiLabelDocument.getDocClasses().contains(label))? 1:0);
				if (biggerClass == document.getDocClass()) {
					documentsOfBiggerClass.add(document);
				}
				else {
					documentsOfSmallerClass.add(document);
				}
			}
			Corpus corpus = new Corpus();
			corpus.setLabel(label);
			List<Document> corpusDocuments = new ArrayList<Document>();
			corpusDocuments.addAll(documentsOfSmallerClass);
			corpusDocuments.addAll(documentsOfBiggerClass.subList(0, documentsOfSmallerClass.size()));
			corpus.setDocuments(corpusDocuments);
			multiLabeledCorpora.add(corpus);
			//this is a binary classification task we don't need to create the same corpora twice
			if (corporaStatistics.keySet().size()==2) {
				break;
			}
		}
	}
	/**
	 * Train the models using all the previously created corpora
	 * @throws Exception
	 */
	private void trainModels() throws Exception{
		for (Corpus corpus : multiLabeledCorpora) {
			Classifier classifier = new Classifier(configuration);
			Model model = classifier.buildModel(corpus);
			model.setLabel(corpus.getLabel());
			multiLabeledModels.add(model);
		}
	}
	/**
	 * Get the predictions from each previously created model for each document
	 * @throws Exception
	 */
	private void getPredictions() throws Exception{
		HashMap<String, List<MultiLabelPrediction>> predictions = new HashMap<>();
		//get the predictions using all the models
		for (Model model : multiLabeledModels) {
			Classifier classifier = new Classifier(configuration);
			Corpus currentCorpus = new Corpus();
			for (MultiLabelDocument multiLabelDocument : originalCorpus.getDocuments()) {
				Document document = new Document(multiLabelDocument.getText());
				document.setId(multiLabelDocument.getId());
				currentCorpus.addDocument(document);
			}
			currentCorpus = classifier.applyModel(currentCorpus, model);
			//add the prediction to predictions for all the documents
			for (Document document : currentCorpus.getDocuments()) {
				MultiLabelPrediction multiLabelPrediction = new MultiLabelPrediction(model.getLabel(), document.getUsedFeatures(), document.getModelBasedClass(), document.getConfidence(), document.getDocClass());
				List<MultiLabelPrediction> currentPredictions = (predictions.containsKey(document.getId()))?predictions.get(document.getId()):new ArrayList<MultiLabelPrediction>();
				currentPredictions.add(multiLabelPrediction);
				predictions.put(document.getId(), currentPredictions);
			}
		}
		this.predictions = predictions;
	}
	/**
	 * This method merges all the predictions for all the documents
	 */
	private void mergePredictions(){
		HashMap<String, HashMap<Integer, Double>> mergedPredictions = new HashMap<>();
		for (String documentID : predictions.keySet()) {
			HashMap<Integer, Double> weightsSums = new HashMap<Integer, Double>();
			HashMap<Integer, Double> confidencesSums = new HashMap<Integer, Double>();
			for (MultiLabelPrediction multiLabelPrediction : predictions.get(documentID)) {
				double currentValue = (weightsSums.containsKey(multiLabelPrediction.getLabel()))?weightsSums.get(multiLabelPrediction.getLabel()):0;
				weightsSums.put(multiLabelPrediction.getLabel(), currentValue + multiLabelPrediction.getWeight());
				
				Double confidence = (multiLabelPrediction.getClassPrediction()==1)?multiLabelPrediction.getConfidence():1-multiLabelPrediction.getConfidence();
				
				currentValue = (confidencesSums.containsKey(multiLabelPrediction.getLabel()))?confidencesSums.get(multiLabelPrediction.getLabel()):0;
				confidencesSums.put(multiLabelPrediction.getLabel(), currentValue + confidence*multiLabelPrediction.getWeight());
			}
			HashMap<Integer, Double> modelBasedClases = new HashMap<Integer, Double>();
			for (Integer label : corporaStatistics.keySet()) {
				modelBasedClases.put(label, confidencesSums.get(label)/weightsSums.get(label));
				if (corporaStatistics.keySet().size()==2) {
					modelBasedClases.put((label == 1)? 0:1, 1 - modelBasedClases.get(label));
					break;
				}
			}
			mergedPredictions.put(documentID, modelBasedClases);
		}
		for (MultiLabelDocument document : originalCorpus.getDocuments()) {
			document.setModelBasedClases(mergedPredictions.get(document.getId()));
		}
	}
}
