package sk.koncz.classifier.datamodel;

import java.util.ArrayList;
import java.util.List;

public class MultiLabelCorpus {
	private List<MultiLabelDocument> documents = new ArrayList<MultiLabelDocument>();
	
	public List<MultiLabelDocument> getDocuments() {
		return documents;
	}
	public void setDocuments(List<MultiLabelDocument> documents) {
		this.documents = documents;
	}
	public void addDocument(MultiLabelDocument document) {
		documents.add(document);
	}
}
