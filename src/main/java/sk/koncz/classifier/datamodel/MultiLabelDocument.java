package sk.koncz.classifier.datamodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class MultiLabelDocument {
	private String id; 
	private String text;
	private Set<Integer> docClasses = new HashSet<Integer>();
	private HashMap<Integer, Double> modelBasedClases;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public MultiLabelDocument(String text) {
		this.text=text;
		this.id = UUID.randomUUID().toString();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id=id;
	}
	public HashMap<Integer, Double> getModelBasedClases() {
		return modelBasedClases;
	}
	public void setModelBasedClases(HashMap<Integer, Double> modelBasedClases) {
		this.modelBasedClases = modelBasedClases;
	}
	public Set<Integer> getDocClasses() {
		return docClasses;
	}
	public void setDocClasses(Set<Integer> docClasses) {
		this.docClasses = docClasses;
	}
	public void setDocClass(Integer docClass) {
		this.docClasses.add(docClass);
	}
	public List<Integer> getModelBasedTrueClases() {
		List<Integer> modelBasedTrueClasses = new ArrayList<Integer>();
		Set<Integer> keys = this.modelBasedClases.keySet();
		for (Integer key : keys) {
			if (this.modelBasedClases.get(key)>0.5) modelBasedTrueClasses.add(key);
		}
		return modelBasedTrueClasses;
	}
}
