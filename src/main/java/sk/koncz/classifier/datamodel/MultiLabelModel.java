package sk.koncz.classifier.datamodel;


public class MultiLabelModel extends Model{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6340143103030919878L;
	private Integer label;

	public Integer getLabel() {
		return label;
	}

	public void setLabel(Integer label) {
		this.label = label;
	}
}
