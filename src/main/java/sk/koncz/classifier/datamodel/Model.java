package sk.koncz.classifier.datamodel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class Model implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2602643734856226222L;
	private HashMap<String, Feature> characterNGrams = new HashMap<>();
	private HashMap<String, Feature> prefixes = new HashMap<>();
	private HashMap<String, Feature> suffixes = new HashMap<>();
	private HashMap<String, Feature> wholeTokens = new HashMap<>();
	private HashMap<String, Feature> features = new HashMap<>();
	private Double tresholdWeightsPositive;
	private Double tresholdWeightsNegative;
	private Double beta0;
	private Double beta1;
	private Double beta2;
	private Double beta3;
	private Double beta4;
	private Double treshold;
	private Double estimatedAccuracy;
	private Integer label;
	private Configuration configuration;
	
	public HashMap<String, Feature> getFeatures() {
		return features;
	}
	public void setFeatures(HashMap<String, Feature> features) {
		this.features = features;
	}
	public Double getTreshold() {
		return treshold;
	}
	public void setTreshold(Double treshold) {
		this.treshold = treshold;
	}
	public Model() {
		
	}
	public void save(String path) {
		try {
			File file = new File(path);
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.close();

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public Model(File model) {
		
	}
	public HashMap<String, Feature> getCharacterNGrams() {
		return characterNGrams;
	}
	public void setCharacterNGrams(HashMap<String, Feature> characterNGrams) {
		this.characterNGrams = characterNGrams;
	}
	public Double getTresholdWeightsPositive() {
		return tresholdWeightsPositive;
	}
	public void setTresholdWeightsPositive(Double tresholdWeightsPositive) {
		this.tresholdWeightsPositive = tresholdWeightsPositive;
	}
	public Double getTresholdWeightsNegative() {
		return tresholdWeightsNegative;
	}
	public void setTresholdWeightsNegative(Double tresholdWeightsNegative) {
		this.tresholdWeightsNegative = tresholdWeightsNegative;
	}
	public Double getEstimatedAccuracy() {
		return estimatedAccuracy;
	}
	public void setEstimatedAccuracy(Double estimatedAccuracy) {
		this.estimatedAccuracy = estimatedAccuracy;
	}
	public HashMap<String, Feature> getPrefixes() {
		return prefixes;
	}
	public void setPrefixes(HashMap<String, Feature> prefixes) {
		this.prefixes = prefixes;
	}
	public HashMap<String, Feature> getSuffixes() {
		return suffixes;
	}
	public void setSuffixes(HashMap<String, Feature> suffixes) {
		this.suffixes = suffixes;
	}
	public HashMap<String, Feature> getWholeTokens() {
		return wholeTokens;
	}
	public void setWholeTokens(HashMap<String, Feature> wholeTokens) {
		this.wholeTokens = wholeTokens;
	}
	public Double getBeta0() {
		return beta0;
	}
	public void setBeta0(Double beta0) {
		this.beta0 = beta0;
	}
	public Double getBeta1() {
		return beta1;
	}
	public void setBeta1(Double beta1) {
		this.beta1 = beta1;
	}
	public Double getBeta2() {
		return beta2;
	}
	public void setBeta2(Double beta2) {
		this.beta2 = beta2;
	}
	public Double getBeta3() {
		return beta3;
	}
	public void setBeta3(Double beta3) {
		this.beta3 = beta3;
	}
	public Double getBeta4() {
		return beta4;
	}
	public void setBeta4(Double beta4) {
		this.beta4 = beta4;
	}
	public Integer getLabel() {
		return label;
	}
	public void setLabel(Integer label) {
		this.label = label;
	}
	public Configuration getConfiguration() {
		return configuration;
	}
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
}
